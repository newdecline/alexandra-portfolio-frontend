import React from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import {setAllProjects} from '../redux/actions/actionCreators';
import ArrowIcon from "../svg/icon-arrow-bold.svg";
import scrollIntoView from "scroll-into-view";

export const AllProjectsButton = props => {
    const {disabled, projectListRef} = props;

    const dispatch = useDispatch();
    const isShowAllProjects = useSelector(({projects}) => projects.isShowAllProjects);

    const handleClickButton = () => {
        dispatch(setAllProjects(!isShowAllProjects));

        if (projectListRef && isShowAllProjects) {
            scrollIntoView(projectListRef.current, {
                time: 600,
                align: {
                    top: 0,
                    left: 0,
                    topOffset: 200,
                    leftOffset: 0
                },
            })
        }
    };

    return (
        <Button
            disabled={disabled}
            onClick={handleClickButton}>
            Все проекты
            <ArrowIconButton open={isShowAllProjects}>
                <ArrowIcon/>
            </ArrowIconButton>
        </Button>
    )
};


const Button = styled.button.attrs(({disabled}) => {
    disabled: `${disabled}`
})`
  @media (min-width: 320px) {
    display: flex;
    align-items: center;
    justify-content: center;
    width: calc(100% - 32px);
    padding: 11px 0;
    margin: 16px auto;
    font-size: 20px;
    line-height: 29px;
    color: #fff;
    box-sizing: border-box;
    background-color: #1ABC9C;
    border: none;
    border-radius: 5px;
    &:disabled {
      background-color: #ccc;
    }
  }
  @media (min-width: 1024px) {
    width: auto;
    margin: 30px auto;
    padding: 11px 73px;
    &:hover {
      cursor: pointer;
    }
  }
`;

const ArrowIconButton = styled.span`
  @media (min-width: 320px) {
    display: flex;
    width: 9px;
    height: 5px;
    margin-left: 7px;
    transform: ${({open}) => open ? "scale(1, -1)" : "scale(1, 1)"};
    transition: transform .6s;
    svg {
      width: 100%;
      height: auto;
      path {
        stroke: #fff;
        fill: #fff;
        stroke-width: 0.6;
      }
    }
  }
`;