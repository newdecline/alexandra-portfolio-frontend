import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

export const Collapsing = props => {
    const {children, isCollapsed, startHeight, classes = []} = props;

    const [contentHeight, setContentHeight] = useState(0);

    useEffect(() => {
        const content = children.ref.current;


        if (content && !contentHeight) {
            let elHeight = content.offsetHeight;

            elHeight += parseInt(window.getComputedStyle(content).getPropertyValue('margin-top'));
            elHeight += parseInt(window.getComputedStyle(content).getPropertyValue('margin-bottom'));

            setContentHeight(elHeight);
        }


    }, [children.ref]);

    return (
        <Wrapper
            isCollapsed={isCollapsed}
            contentHeight={contentHeight}
            startHeight={startHeight}
            className={classes.join(' ')}>
            {children}
        </Wrapper>
    )
};

const Wrapper = styled.div`
  @media (min-width: 320px) {
    height: ${({isCollapsed, contentHeight, startHeight = 0}) => isCollapsed ? `${contentHeight}px` : `${startHeight}px`};
    transition: height .6s ease-in-out;
    overflow: hidden;
  }
  @media (min-width: 1024px) {
    &.drop-down-list {
      height: auto;
      overflow: unset;
    }
  }
`;