import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import {SectionHeader} from "./SectionHeader";
import {Form} from "./Form";
import VKIcon from "../svg/icon-vk.svg";
import BeehanceIcon from "../svg/icon-beehance.svg";
import InstagramIcon from "../svg/icon-instagram.svg";
import WhatsapIcon from "../svg/icon-whatsap.svg";
import ViberIcon from "../svg/icon-viber.svg";
import DotsIcon from "../svg/icon-dots.svg";
import PhoneIcon from "../svg/icon-phone.svg";
import MailIcon from "../svg/icon-mail.svg";
import {Collapsing} from "./Collapsing";

export const Contacts = props => {
    const iconWrapRef = useRef(null);

    const [isIconsExpanded, setIsIconsExpanded] = useState(false);

    return (
        <ContactsSection data-id-section="contacts">
            <SectionHeader title={'Контакты'}/>

            <div className="wrapper">
                <Form/>

                <AdditionalContacts>
                    <AdditionalContactsHeader>Свяжитесь со мной
                        и мы сможем обсудить вашу задачу уже сегодня!
                    </AdditionalContactsHeader>

                    <div className="wrapper">
                        <div className="phone-and-email-wrapper">
                            <AdditionalContactsPhone href="tel:+79823365168">
                                <span className="icon-wrapper"><PhoneIcon/></span>+7 982 336 51 68
                            </AdditionalContactsPhone>
                            <AdditionalContactsEmail href="mailto:austrahova@gmail.com">
                                <span className="icon-wrapper"><MailIcon/></span>austrahova@gmail.com
                            </AdditionalContactsEmail>
                        </div>

                        <IconWrap>
                            <Collapsing isCollapsed={isIconsExpanded} startHeight={60} classes={['icons-list']}>
                                <IconGrid ref={iconWrapRef}>
                                    <Icon target="_blank" href="https://vk.com/id153726265"><VKIcon/></Icon>
                                    <Icon target="_blank" href="https://www.behance.net/austrahovad9ef"><BeehanceIcon/></Icon>
                                    <Icon target="_blank" href="http://instagram.com/strakhovaaleksandra"><InstagramIcon/></Icon>
                                    <Icon target="_blank" href="https://wa.me/79823365168"><WhatsapIcon/></Icon>
                                    <Icon target="_blank" href="viber://chat?number=79823365168"><ViberIcon/></Icon>
                                </IconGrid>
                            </Collapsing>
                            <DotsIconWrap onClick={() => {setIsIconsExpanded(!isIconsExpanded)}}><DotsIcon/></DotsIconWrap>
                        </IconWrap>
                    </div>
                </AdditionalContacts>
            </div>
        </ContactsSection>
    )
};

const ContactsSection = styled.section`
  @media (min-width: 1366px) {
    .wrapper {
      display: flex;
      justify-content: center;
      max-width: 1366px;
      margin: 0 auto 45px auto;
    }
  }
`;

const AdditionalContacts = styled.div`
  @media (min-width: 1024px) {
    width: 30%;
    .phone-and-email-wrapper {
      display: flex;
      flex-direction: column;
      margin-right: 152px;
    }
    .wrapper {
      display: flex;
      justify-content: center;
      margin-bottom: 47px;
    }
  }
  @media (min-width: 1366px) {
    .wrapper {
      display: flex;
      flex-direction: column;
    }
    .phone-and-email-wrapper {
      margin-right: 0;
      margin-bottom: 30px;
    }
  }
  @media (min-width: 1600px) {
    width: 100%;
  }
`;

const AdditionalContactsHeader = styled.div`
  @media (min-width: 320px) {
    margin: 26px 20px 32px 20px;
    font-size: 20px;
    line-height: 25px;
    font-family: 'Circe-Light';
    text-align: center;
  }
  @media (min-width: 1024px) {
    margin: 47px 20px 32px 20px;
  }
  @media (min-width: 1366px) {
    margin: 47px 20px 32px 0;
    text-align: left;
  }
`;

const AdditionalContactsPhone = styled.a`
  @media (min-width: 320px) {
    display: inline-block;
    width: 100%;
    margin-bottom: 20px;
    text-align: center;
    font-size: 22px;
    line-height: 32px;
    text-decoration: none;
    color: #000;
    .icon-wrapper {
      display: none;
    }
  }
  @media (min-width: 1024px) {
    display: flex;
    align-items: center;
    text-align: left;
    .icon-wrapper {
      display: inline-flex;
      justify-content: flex-start;
      width: 24px;
      height: 23px;
      margin-right: 25px;
      svg {
        width: 100%;
        height: auto;
        path {
          fill: #1ABC9C;
        }
      }
    }
  }
`;

const AdditionalContactsEmail = styled.a`
  @media (min-width: 320px) {
    display: inline-block;
    width: 100%;
    margin-bottom: 20px;
    text-align: center;
    font-size: 22px;
    line-height: 32px;
    text-decoration: none;
    color: #000;
    .icon-wrapper {
      display: none;
    }
  }
  @media (min-width: 1024px) {
    display: flex;
    align-items: center;
    margin-bottom: 0;
    text-align: left;
    .icon-wrapper {
      display: inline-flex;
      justify-content: flex-start;
      width: 27px;
      height: 18px;
      margin-right: 25px;
      svg {
        width: 100%;
        height: auto;
        path {
          fill: #1ABC9C;
        }
      }
    }
  }
`;

const IconWrap = styled.div`
  @media (min-width: 320px) {
    position: relative;
    margin-bottom: 50px;
  }
  @media (min-width: 1024px) {
    margin-bottom: 9px;
    align-self: flex-end;
    .icons-list {
      height: 40px;
    }
  }
  @media (min-width: 1366px) {
    align-self: flex-start;
  }
`;

const IconGrid = styled.div`
  @media (min-width: 320px) {
    position: relative;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    width: 206px;
    margin: 0 auto;
    grid-gap: 28px;
  }
  @media (min-width: 1024px) {
    grid-template-columns: 40px 40px 40px;
    grid-template-rows: 40px 40px 40px;
  }
`;

const Icon = styled.a`
  @media (min-width: 320px) {
    display: block;
    svg {
      width: 100%;
      height: auto;
    }
  }
`;

const DotsIconWrap = styled.button`
  @media (min-width: 320px) {
    position: absolute;
    left: 50%;
    bottom: -30px;
    transform: translate(-50%, 0);
    border: none;
    background-color: transparent;
  }
  @media (min-width: 1024px) {
    display: none;
  }
`;