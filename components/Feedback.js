import React from 'react';
import styled from 'styled-components';
import {SectionHeader} from "./SectionHeader";
import Swiper from "react-id-swiper";
import VkIcon from "../svg/icon-vk.svg";
import {feedback} from "../fakeData/feedbackData";
import { Scrollbars } from 'react-custom-scrollbars';

export const Feedback = props => {
    const initialSettings = {
        slidesPerView: 1,
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 15
            },
            1366: {
                slidesPerView: 3,
                spaceBetween: 0
            },
        }
    };

    return (
        <Section data-id-section="feedback">
            <SectionHeader title={'Отзывы клиентов'}/>

            <Swiper {...initialSettings}>
                {
                    feedback.map((slide, index) => {
                        const {
                            srcImg,
                            personName,
                            company,
                            reviewText,
                            personLink} = slide;

                        return (
                            <Slide key={index}>
                                <div className="inner-wrapper">
                                    <WrapperImg>
                                        <Img src={srcImg}/>
                                    </WrapperImg>

                                    <SlideHeader>
                                        <SlideHeaderSpan>{personName}&nbsp;&nbsp;</SlideHeaderSpan>
                                        {company}
                                    </SlideHeader>

                                    <Scrollbars
                                        universal
                                        autoHeight
                                        autoHeightMin={100}
                                        autoHeightMax={100}
                                        className='scrollbars'>
                                        <p className="slide-text"><span>{reviewText}</span></p>
                                    </Scrollbars>

                                    <SlideFooter>
                                        <a href={personLink} target="_blank" className='link'>
                                            <VkIcon/>
                                        </a>
                                    </SlideFooter>
                                </div>
                            </Slide>
                        );
                    })
                }
            </Swiper>
        </Section>
    )
};

const Section = styled.section`
    max-width: 1366px;
    margin: 0 auto;
    @media (min-width: 1366px) {
        padding: 0 40px;
    }
    @media (min-width: 1600px) {
        padding: 0;
    }
`;


const Slide = styled.div`
    display: flex;
    flex-direction: column;
    .inner-wrapper {
      margin-bottom: 10px;
      box-shadow: 0 3px 10px rgba(0, 0, 0, 0.1);
    }
    .scrollbars {
      :after {
        content: '';
        position: absolute;
        top: 0;
        left: 27px;
        width: 4px;
        height: 100%;
        background-color: #1ABC9C;
      }
    }
    .slide-text {
      padding: 0 22px 0 47px;
      height: 100px;
      font-size: 16px;
      line-height: 24px;
      color: #000;
      box-sizing: border-box;
      overflow-y: auto;
      span {
        white-space: pre-wrap;
      }
    }
    @media (min-width: 1024px) {
        width: 49.3%;
        .slide-text {
            font-size: 18px;
            line-height: 27px;
            overflow-y: unset;
        }
    }
    @media (min-width: 1366px) {
        width: 33.3%; 
        .inner-wrapper {
            margin: 10px; 
        }
    }
`;

const WrapperImg = styled.div`
    position: relative;
    ::before {
        content: '';
        position: absolute;
        bottom: 0;
        right: 0;
        width: 0;
        height: 0;
        border-bottom: solid 25px rgb(255, 255, 255);
        border-right: solid 25px rgb(255, 255, 255);
        border-left: solid 25px transparent;
        border-top: solid 25px transparent;
    }
`;

const Img = styled.img`
    display: block;
    width: 100%;
`;

const SlideHeader = styled.div`
    min-height: 46px;
    margin: 30px 22px 13px 26px;
    font-size: 15px;
    line-height: 22px;
    @media (min-width: 1366px) {
        font-size: 18px;
        line-height: 27px;
    }
`;

const SlideHeaderSpan = styled.span`
    font-size: 16px;
    line-height: 24px;
    color: #1ABC9C;
    text-transform: uppercase;
    @media (min-width: 1366px) {
        font-size: 20px;
        line-height: 29px;
    }
`;

const SlideFooter = styled.div`
    margin: 15px 22px 20px 26px;
`;