import React, { useState } from 'react';
import styled from 'styled-components';
import Swiper from 'react-id-swiper';
import {projectsListFakeData} from '../fakeData/projectList';
import ArrowIcon from '../svg/icon-arrow.svg';
import MailIcon from '../svg/icon-mail.svg';
import PhoneIcon from '../svg/icon-phone.svg';
import Link from "next/link";

export const Footer = props => {
    const [gallerySlider, updateGallerySlider] = useState(null);

    const initialSettings = {
        getSwiper: updateGallerySlider,
        slidesPerView: 1.3,
        loop: true,
        centeredSlides: true,
        spaceBetween: 7,
        breakpoints: {
            1024: {
                slidesPerView: 3.5,
            },
            1366: {
                slidesPerView: 5.5,
            }
        }
    };

    const goNext = () => {
        if (gallerySlider !== null) {
            gallerySlider.slideNext();
        }
    };

    const goPrev = () => {
        if (gallerySlider !== null) {
            gallerySlider.slidePrev();
        }
    };

    return (
        <StyledFooter>
            <SliderWrap>
                <Swiper {...initialSettings}>
                    {
                        projectsListFakeData.map((project, index) => {
                            const {slug, preview, alt} = project;

                            return (
                                <Slide key={index}>
                                    <Link
                                        href={{
                                            pathname: '/project',
                                            query: {slug}
                                        }}
                                        as={`/proekt/${slug}`}>
                                        <a>
                                            <ImgWrap>
                                                <img src={preview} alt={alt}/>
                                            </ImgWrap>
                                        </a>
                                    </Link>

                                </Slide>
                            )
                        })
                    }
                </Swiper>
                <ButtonArrow className="swiper-button-next" onClick={goNext}><ArrowIcon/></ButtonArrow>
                <ButtonArrow className="swiper-button-prev" onClick={goPrev}><ArrowIcon/></ButtonArrow>
            </SliderWrap>

            <FooterBottomWrap>
                <FooterBottomLinkWrap>
                    <FooterBottomLink href="mailto:austrahova@gmail.com">
                        <MailIcon/>
                        austrahova@gmail.com
                    </FooterBottomLink>
                    <FooterBottomLink href="tel:+79823365168">
                        <PhoneIcon/>
                        +7 982 336 51 68
                    </FooterBottomLink>
                </FooterBottomLinkWrap>
                <Copywriter>© Все права защищены 2019 г.</Copywriter>
            </FooterBottomWrap>
        </StyledFooter>
    );
};

const StyledFooter = styled.footer`
  @media (min-width: 320px) {
    position: relative;
    overflow-x: hidden;
  }
`;

const SliderWrap = styled.div`
  @media (min-width: 320px) {
    position: relative;
  }
`;

const Slide = styled.div`
  @media (min-width: 320px) {
    width: 76.7%;
    a {
      display: block;
    }
  }
  @media (min-width: 1024px) {
    width: 28.1%;
  }
  @media (min-width: 1380px) {
    width: 17.8%;
  }
`;

const ImgWrap = styled.div`
  @media (min-width: 320px) {
    img {
      display: block;
      width: 100%;
    }
  }
`;

const ButtonArrow = styled.button`
  @media (min-width: 320px) {
    position: absolute;
    top: 50%;
    left: -24px;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 59px;
    height: 59px;
    margin-top: -28px;
    padding: 0;
    background-color: #fff;
    border: none;
    border-radius: 50%;
    transform: rotate(90deg);
    box-sizing: border-box;
    :nth-child(2) {
      right: -24px;
      left: unset;
      transform: rotate(-90deg);
    }
    svg {
      width: 26px;
      height: 13px;
      transform: translate(10px, 0);
      :last-child {
        transform: translate(0, -9px);
      }
    }
  }
`;

const FooterBottomWrap = styled.div`
  @media (min-width: 320px) {
   padding: 25px 8px;
   box-sizing: border-box;
  }
  @media (min-width: 1024px) {
    display: flex;
    justify-content: center;
    padding: 97px 0;
  }
`;

const FooterBottomLinkWrap = styled.div`
  @media (min-width: 320px) {
    margin: 0 auto 10px auto;
    text-align: center;
  }
  @media (min-width: 1024px) {
    margin: 0 251px 0 0;
  }
`;

const FooterBottomLink = styled.a`
  @media (min-width: 320px) {
    display: inline-flex;
    align-items: center;
    font-size: 14px;
    line-height: 21px;
    color: #B5B5B5;
    text-decoration: none;
    :nth-child(1) {
      margin-right: 10px;
    }
    svg {
      padding-right: 8px;
      path {
        fill: #B5B5B5;
      }
    }
  }
  @media (min-width: 1024px) {
    color: #000;
    :nth-child(1) {
      margin-right: 20px;
    }
    svg {
      path {
        fill: #000
      }
    }
  }
`;

const Copywriter  = styled.div`
  @media (min-width: 320px) {
    text-align: center;
    font-size: 14px;
    line-height: 21px;
    color: #B5B5B5;
  }
  @media (min-width: 1024px) {
    color: #000;
  }
`;