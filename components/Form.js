import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';
import {Field, Formik} from "formik";
import * as Yup from 'yup';
import {enablePageScroll, disablePageScroll} from 'scroll-lock';
import {fetchApi} from "../services/api/fetchApi";
import {CustomTextarea} from "./Textarea";
import PhoneIcon from "../svg/icon-phone.svg";
import MailIcon from "../svg/icon-mail.svg";

const FeedbackSchema = Yup.object().shape({
    name: Yup.string().min(2).max(50).required(),
    email: Yup.string().email().required(),
    agreement: Yup.bool().oneOf([true])
});

export const Form = props => {
    const refSpanAttachFile = useRef(null);

    const [isSendForm, setSendForm] = useState(false);
    const [isErrorSendForm, setErrorSendForm] = useState(false);
    const [isSubmittingForm, setIsSubmittingForm] = useState(false);
    const [dataInputFile, setDataInputFile] = useState(undefined);

    useEffect(() => {
        if (isSendForm || isErrorSendForm) {
            disablePageScroll();
        } else {
            enablePageScroll();
        }
    }, [isSendForm, isErrorSendForm]);

    const handleChangeInputFile = e => {
        setDataInputFile(e.target.files[0]);
    };

    useEffect(() => {
        const nameAttachFile = dataInputFile && dataInputFile.name.replace(/\.[^.]*$/,'');

        if (dataInputFile) {
            if (nameAttachFile.length > 20) {
                refSpanAttachFile.current.innerText = nameAttachFile.slice(0, 20) + ' ...';
            } else {
                refSpanAttachFile.current.innerText = nameAttachFile;
            }

        }

    }, [dataInputFile]);

    return (
        <Formik
            initialValues={{
                name: '',
                email: '',
                message: '',
                agreement: false }}

            validationSchema={FeedbackSchema}

            onSubmit={(values, { resetForm } ) => {
                const formData = new FormData();

                setIsSubmittingForm(true);

                formData.append('name', values.name);
                formData.append('email', values.email);
                formData.append('message', values.message);
                formData.append('file', dataInputFile);

                fetchApi('/send-feedback-form', {
                    headers: {},
                    method: 'post',
                    body: formData,
                })
                .then((res) => {
                    if (!res.status.ok) {
                        setErrorSendForm(true);
                        setIsSubmittingForm(false);
                        setDataInputFile(undefined);
                        resetForm();
                        console.log(res);
                    } else {
                        setSendForm(true);
                        setIsSubmittingForm(false);
                        setDataInputFile(undefined);
                        resetForm();
                    }
                })
                .catch(err => {
                    setErrorSendForm(true);
                    setIsSubmittingForm(false);
                    setDataInputFile(undefined);
                    resetForm();
                    console.log(err);
                });
            }}

            render={props => {
                const {
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleSubmit,
                } = props;

                if (isErrorSendForm) {
                    return (
                        <ErrorSubmissionMessage>
                            <h3 className="form-error__header">Извините, <br/>ваше сообщение не отправлено!</h3>
                            <p className="form-error__text">Попробуйте позже или свяжитесь со мной другим способом.</p>
                            <div className="wrapper">
                                <a
                                    className="link phone"
                                    href="tel:+79823365168">
                                    <span className='icon'><PhoneIcon/></span>
                                    <span className='phone__text'>+7 982 336 51 68</span></a>

                                <a
                                    className="link mail"
                                    href="mailto:austrahova@gmail.com">
                                    <span className='icon'><MailIcon/></span>
                                    <span className='phone__text'>austrahova@gmail.com</span></a>
                            </div>
                            <button
                                onClick={() => setErrorSendForm(false)}
                                className="form-error__close">Закрыть</button>
                        </ErrorSubmissionMessage>
                    )
                }

                if (isSendForm) {
                    return (
                        <SuccessfulSubmissionMessage>
                            <div className="inner">
                                <img src="/success-form-img.png" alt="success send form"/>
                                <h3>Спасибо!</h3>
                                <p>Ваше сообщение успешно отправлено.</p>
                                <p>Я отвечу вам в ближайшее время</p>
                                <button
                                    onClick={() => {
                                        setIsSubmittingForm(false);
                                        setSendForm(false)}
                                    }
                                >Закрыть
                                </button>
                            </div>
                        </SuccessfulSubmissionMessage>
                    )
                }

                return (
                    <StyledForm
                        onSubmit={handleSubmit}
                        encType="multipart/form-data">
                        <FormTitle>Заказать дизайн</FormTitle>

                        <GridForm>
                            <FormInput
                                onChange={handleChange}
                                placeholder='Ваше имя*'
                                type="text"
                                name='name'
                                value={values.name}
                                validateErr={errors.name && touched.name}/>
                            <FormInput
                                onChange={handleChange}
                                placeholder='Ваш @mail*'
                                type="text"
                                validateErr={errors.email && touched.email}
                                name='email'
                                value={values.email}/>
                            <Field
                                component={CustomTextarea}
                                name="message"/>
                            <AttachFileLabel>
                                <AttachFileLabelText ref={refSpanAttachFile}>Прикрепить бриф +</AttachFileLabelText>
                                <AttachFileInput
                                    onChange={handleChangeInputFile}
                                    type="file"
                                    name='file'/>
                            </AttachFileLabel>
                            <SubmitFormButton
                                disabled={isSubmittingForm}
                                type='submit'>{isSubmittingForm ? '' : 'Отправить'}
                            </SubmitFormButton>
                            <AgreementLabel>
                                <AgreementInput
                                    onChange={handleChange}
                                    checked={values.agreement}
                                    type="checkbox"
                                    name='agreement'/>
                                <AgreementCircleIcon validateErr={errors.agreement && touched.agreement}> </AgreementCircleIcon>
                                <AgreementText>Согласие с обработкой персональных данных</AgreementText>
                            </AgreementLabel>
                        </GridForm>
                    </StyledForm>
                )
            }}
        />
    )
};

const StyledForm = styled.form`
  @media (min-width: 320px) {
    padding: 23px 11px 30px;
    background: #9400AC;
    color: #fff;
    box-sizing: border-box;
    .form__textarea {
      padding: 18px 0 18px 15px;
      border: none;
      border-radius: 5px;
      background: #fff;
      font-size: 16px;
      line-height: 24px;
      color: #8C8C8C;
      box-sizing: border-box;
      transition: height .3s;
      font-family: 'Circe-Regular';
      resize: none;
      min-height: 108px;
    }
  }
  @media (min-width: 1024px) {
    padding: 45px 97px;
    .form__textarea {
      min-height: 100%;
      grid-area: textarea;
    }
  }
  @media (min-width: 1366px) {
    width: 50%;
    padding: 47px;
    margin-right: 60px;
    border-radius: 9px;
  }
  @media (min-width: 1600px) {
    min-width: 790px;
  }
`;

const FormTitle = styled.h3`
  @media (min-width: 320px) {
    margin-bottom: 23px;
    font-size: 20px;
    line-height: 29px;
    letter-spacing: 0.07em;
    text-align: center;
  }
  @media (min-width: 1024px) {
    font-size: 24px;
  }
`;

const GridForm = styled.div`
  @media (min-width: 320px) {
    display: grid;
    grid-row-gap: 12px;
  }
  @media (min-width: 1024px) {
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr 1fr 1fr auto;
    grid-column-gap: 28px;
    grid-template-areas:
        "name textarea"
        "email textarea"
        "attach-file submit"
        ". agreement";
  }
`;

const FormInput = styled.input`
  @media (min-width: 320px) {
    padding: 18px 0 18px 15px;
    border: none;
    border-radius: 5px;
    background: ${({validateErr}) => validateErr ? '#ffa5b0' : '#fff'};
    font-size: 16px;
    line-height: 24px;
    color: #8C8C8C;
    box-sizing: border-box;
  }
  @media (min-width: 1024px) {
    [name='name'] {
      grid-area: name;
    }
    [name='email'] {
      grid-area: email;
    }
  }
`;

const AttachFileLabel = styled.label`
  @media (min-width: 320px) {
    position: relative;
    padding: 18px 0 18px 15px;
    border: 1px solid #FFFFFF;
    border-radius: 5px;
    font-size: 16px;
    line-height: 24px;
    background-color: transparent;
    color: #fff;
    box-sizing: border-box;
    text-align: center;
  }
  @media (min-width: 1024px) {
    grid-area: attach-file;
    :hover {
      cursor: pointer;
    }
  }
`;

const AttachFileLabelText = styled.span`
  @media (min-width: 1024px) {
    
  }
`;

const AttachFileInput = styled.input`
  @media (min-width: 320px) {
    position: absolute;
    top: 0;
    left: 0;
    visibility: hidden;
    z-index: 0;
  }
`;

const SubmitFormButton = styled.button.attrs(({disabled}) => {
    disabled: `${disabled}`
})`
  @media (min-width: 320px) {
    height: ${({disabled}) => disabled ? '60px' : 'unset'};
    padding: 18px 0 18px 15px;
    border: none;
    border-radius: 5px;
    font-size: 16px;
    line-height: 24px;
    background: #1ABC9C;
    color: #fff;
    box-sizing: border-box;
    text-align: center;
    text-transform: uppercase;
    &:disabled {
      background: url('/preloader.gif');
      background-repeat: no-repeat;
      background-position: center;
      background-size: 7%;
    }
  }
  @media (min-width: 1024px) {
    grid-area: submit;
    :hover {
      cursor: pointer;
    }
  }
`;

const AgreementLabel = styled.label`
  @media (min-width: 320px) {
    position: relative;
    display: block;
    padding-left: 26px;
    box-sizing: border-box;
  }
  @media (min-width: 1024px) {
    grid-area: agreement;
    :hover {
      cursor: pointer;
    }
  }
`;

const AgreementInput = styled.input`
  @media (min-width: 320px) {
    position: absolute;
    top: 0;
    left: 0;
    visibility: hidden;
    z-index: 0;
    :checked + span {
      background-color: #1abc9c;
    }
  }
`;

const AgreementCircleIcon = styled.span`
  @media (min-width: 320px) {
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    width: 16px;
    height: 16px;
    background: ${({validateErr}) => validateErr ? '#ffa5b0' : '#fff'};
    border-radius: 50%;
    transition: background-color .6s;
  }
`;

const AgreementText = styled.span`
  @media (min-width: 320px) {
    font-size: 14px;
    line-height: 21px;
  }
`;

const SuccessfulSubmissionMessage = styled.div`
  @media (min-width: 320px) {
    position: fixed;
    z-index: 3;
    top: 0;
    left: 0;
    display: flex;
    width: calc(var(--vw, 1vw) * 100);
    min-height: calc(var(--vh, 1vh) * 100);
    background-color: #9400AC;
    overflow-y: auto;
    .inner {
      position: absolute;
      top: 50%;
      left: 0;
      width: 100%;
      display: flex;
      align-self: center;
      padding: 100px 15px;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      box-sizing: border-box;
      transform: translate(0, -50%);
    }
    img {
      width: 201px;
      height: auto;
      display: block;
      margin-bottom: 40px;
    }
    h3 {
      margin-bottom: 10px;
      font-size: 22px;
      line-height: 26px;
      text-align: center;
      text-transform: uppercase;
      color: #FFFFFF;
    }
    p {
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #FFFFFF;
    }
    button {
      width: 224px;
      padding: 0;
      margin-top: 30px;
      font-size: 18px;
      line-height: 58px;
      text-transform: uppercase;
      color: #FFFFFF;
      background: #1ABC9C;
      border: none;
      border-radius: 5px;
    }
  }
  @media (min-width: 1024px) {
    width: 100%;
    img {
      width: 326px;
      margin-bottom: 54px;
    }
    h3 {
      font-size: 35px;
      line-height: 52px;
      font-weight: 700;
    }
    p {
      font-size: 30px;
      line-height: 44px;
    }
    button {
      :hover {
        cursor: pointer;
      }
    }
  }
`;

const ErrorSubmissionMessage = styled.div`
  position: fixed;
  z-index: 3;
  top: 0;
  left: 0;
  padding: 30px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: calc(var(--vw, 1vw) * 100);
  min-height: calc(var(--vh, 1vh) * 100);
  background-color: #9400AC;
  color: #fff;
  overflow-y: auto;
  box-sizing: border-box;
  .form-error__header {
    font-size: 20px;
    line-height: 30px;
    text-align: center;
  }
  .form-error__text {
    margin: 15px 0;
    font-size: 20px;
    line-height: 30px;
    text-align: center;
  }
  .wrapper {
    margin: 30px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .link {
    display: flex;
    align-items: center;
    font-size: 20px;
    line-height: 30px;
    color: #fff;
    text-decoration: none;
  }
  .icon {
    width: 20px;
    height: 20px;
    display: flex;
    margin-right: 15px;
    svg {
      width: 100%;
      height: 100%;
      path {
        fill: #fff;
      }
    }
  }
  .form-error__close {
    height: unset;
    padding: 18px 90px 18px 90px;
    border: none;
    border-radius: 5px;
    font-size: 16px;
    line-height: 24px;
    background: #1ABC9C;
    color: #fff;
    box-sizing: border-box;
    text-align: center;
    text-transform: uppercase;
  }
  @media (min-width: 1024px) {
    .form-error__header {
      font-size: 30px;
      line-height: 44px;
    }
    .wrapper {
      margin: 50px 0;
    }
    .form-error__text, .link {
      font-size: 30px;
      line-height: 44px;
    }
    .icon {
      width: 30px;
      height: 30px;
    }
    .form-error__close {
      :hover {
        cursor: pointer;
      }
    }
  }
`;