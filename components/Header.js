import React, { useState } from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import {MenuNavigation} from "./MenuNavigation";
import LogoIcon from '../svg/icon-logo.svg';

export const Header = () => {
    const [isOpenMenu, setOpenMenu] = useState(false);

    return (
        <StyledHeader open={isOpenMenu}>
            <span className="bottom-line-with-shadow"/>
            <Link href='/'>
                <a className="link-logo"><LogoIcon/></a>
            </Link>
            <MenuNavigation
                isOpenMenu={isOpenMenu}
                setOpenMenu={setOpenMenu}/>
            <button
                className="burger-button"
                onClick={() => {setOpenMenu(!isOpenMenu)}}>
                <span> </span>
            </button>
        </StyledHeader>
    )
};

const StyledHeader = styled.header`
  position: fixed;
  top: 0;
  width: 100%;
  padding: 15px 17px 11px 17px;
  background-color: #fff;
  box-sizing: border-box;
  z-index: 1;
  .burger-button {
  position: absolute;
  top: 20px;
  right: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 27px;
  padding: 0;
  border: 0;
  background-color: transparent;
  z-index: 1;
  span {
    position: relative;
    display: block;
    width: 100%;
    height: 3px;
    background-color: ${({open}) => open ? "transparent" : "#000"};
    border-radius: 3px;
    transition: background-color .6s;
    ::before, ::after {
      content: '';
      position: absolute;
      left: 0;
      display: block;
      width: 100%;
      height: 3px;
      background-color: #000;
      border-radius: 3px;
      transition: transform .6s ease-in-out, top .6s ease-in-out;
    }
    ::before {
      top: -12px;
      transform: ${({open}) => open ? "rotate(45deg) translate3d(9px, 8px, 0)" : "rotate(0) translate3d(0, 0, 0)"};
    }
    ::after {
      top: 12px;
      transform: ${({open}) => open ? "rotate(-45deg) translate3d(8px, -10px, 0)" : "rotate(0) translate3d(0, 0, 0)"};
    }
  }
}
  .bottom-line-with-shadow {
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    width: 100%;
    height: 100%;
    box-shadow: 0 2px 15px rgba(0, 0, 0, 0.1);
    z-index: 2;
    pointer-events: none;
  }
  .link-logo {
    position: relative;
    display: inline-block;
    width: 116px;
    height: 34px;
    z-index: 2;
    svg {
      width: 100%;
      height: 100%;
    }
  }
  @media (min-width: 1024px) {
    display: flex;
    align-items: center;
    padding: 20px 30px;
    .burger-button {
      top: 29px;
      right: 30px;
      :hover {
        cursor: pointer;
      }
    }
    .link-logo {
      width: 171px;
      height: 45px;
      :hover {
        cursor: pointer;
      }
    }
  }
`;