import React from "react";
import {Header} from "../Header";
import {Footer} from "../Footer";
import Head from "next/head";

export const MainLayout = props => {
    const {children} = props;

    return (
        <>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <link rel="icon" type="image/x-icon" href="/favicon.png" />
                <title>Портфолио дизайнера Александры Страховой</title>
                <meta
                    name="description"
                    content="Создам дизайн любого уровня"/>
                <meta
                    name="keywords"
                    content="Дизайн, графический дизайнер, дизайнер, дизайнер в Челябинске, заказать логотип, заказать сайт, заказать визитку, закать листовку, заказать баннер, наружняя реклама"/>
            </Head>

            <Header/>

            {children}

            <Footer/>
        </>
    )
};