import React from "react";
import {PageProjectHeader} from "../PageProjectHeader";
import {Footer} from "../Footer";

export const PageProjectLayout = props => {
    const {children} = props;

    return (
        <>
            <PageProjectHeader/>

            {children}

            <Footer/>
        </>
    )
};