import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import {SocialLinkList} from "./SocialLinkList";
import { enablePageScroll, disablePageScroll } from 'scroll-lock';
import scrollIntoView from 'scroll-into-view';
import { useDispatch } from 'react-redux';
import {setPageIndexOffsetBeforeChange, setScrollTo} from "../redux/actions/actionCreators";

export const MenuNavigation = props => {
    const {isOpenMenu, setOpenMenu} = props;

    const menuNavigationRef = useRef(null);

    const router = useRouter();
    const dispatch = useDispatch();

    const handleClickMenuItem = (e) => {
        const anchorId = e.target.dataset.anchorId;

        setOpenMenu(false);

        const target = document.querySelector(`[data-id-section=${anchorId}]`);

        if (router.pathname === '/') {
            scrollIntoView(target, {
                time: 500,
                align: {
                    top: 0,
                    left: 0,
                    topOffset: 70,
                    leftOffset: 0
                }
            });
        } else {
            dispatch(setPageIndexOffsetBeforeChange(0));
            dispatch(setScrollTo(anchorId));
            router.push('/');
        }
    };

    useEffect(() => {
        if (isOpenMenu) {
            disablePageScroll();

            if (menuNavigationRef.current) {
                menuNavigationRef.current.style.margin = `0 17px 0 auto`
            }
        } else {
            enablePageScroll();

            if (menuNavigationRef.current) {
                menuNavigationRef.current.style.margin = `0 0 0 auto`
            }
        }
    }, [isOpenMenu]);

    return (
        <StyledMenuNavigation open={isOpenMenu} ref={menuNavigationRef}>
            <ul className="menu-list">
                {
                    menuList.map((link, index) => {
                        const {label, href} = link;

                        return (
                            <li className="menu-list__item" key={index}>
                                <a
                                    data-anchor-id={href}
                                    className='link'
                                    onClick={(e) => handleClickMenuItem(e)}
                                >{label}</a>
                            </li>
                        )
                    })
                }

                <SocialLinkList isOpenMenu={isOpenMenu}/>
            </ul>
        </StyledMenuNavigation>
    )
};

const StyledMenuNavigation = styled.div`
  position: fixed;
  top: 0;
  left: ${({open}) => open ? "0" : "100%"};
  width: 100%;
  height: calc(var(--vh, 1vh) * 100);
  background-color: #fff;
  overflow-y: auto;
  transition: left .6s ease-in-out;
  .menu-list {
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    min-height: 100%;
    padding: 65px 0 30px 0;
    box-sizing: border-box;
    z-index: 1;
    &__item {
      margin-bottom: 25px;
    }
    .link {
      display: block;
      font-size: 20px;
      line-height: 29px;
      text-decoration: none;
      text-transform: uppercase;
      box-sizing: border-box;
      color: #000;
      &.active {
        border-bottom: 2px solid #1ABC9C;
        color: #1ABC9C;
      }
    }
  }
  @media (min-width: 1024px) {
    position: static;
    width: unset;
    height: unset;
    flex-direction: row;
    margin-left: auto;
    .menu-list {
      position: static;
      flex-direction: row;
      padding: 0;
      &__item {
        margin: 0 55px 0 55px;
        :hover {
          cursor: pointer;
        }
        :last-of-type {
          margin-right: 115px;
        }
      }
    }
  }
`;

const menuList = [
    {
        label: 'Проекты',
        href: 'projects'
    },
    {
        label: 'Отзывы',
        href: 'feedback'
    },
    {
        label: 'Контакты',
        href: 'contacts'
    },
];