import React, { useState } from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import LogoIcon from '../svg/icon-logo.svg';
import {MenuNavigation} from "./MenuNavigation";

export const PageProjectHeader = props => {
    const [isOpenMenu, setOpenMenu] = useState(false);

    return (
        <StyledHeader open={isOpenMenu}>
            <Link href='/'>
                <LinkLogo>
                    <LogoIcon/>
                </LinkLogo>
            </Link>
            <MenuNavigation
                isOpenMenu={isOpenMenu}
                setOpenMenu={setOpenMenu}/>
            <button
                className="burger-button"
                onClick={() => {setOpenMenu(!isOpenMenu)}}>
                <span> </span>
            </button>
            <Link href='/'>
                <a className="link-close">
                    <span> </span>
                </a>
            </Link>
        </StyledHeader>
    )
};

const StyledHeader = styled.header`
position: fixed;
top: 0;
width: 100%;
padding: 15px 17px 11px 17px;
background-color: #fff;
box-sizing: border-box;
z-index: 1;
.burger-button {
  display: none;
  position: absolute;
  top: 20px;
  right: 18px;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 27px;
  padding: 0;
  border: 0;
  background-color: transparent;
  z-index: 1;
  span {
    position: relative;
    display: block;
    width: 100%;
    height: 3px;
    background-color: ${({open}) => open ? "transparent" : "#000"};
    border-radius: 3px;
    transition: background-color .6s;
    ::before, ::after {
      content: '';
      position: absolute;
      left: 0;
      display: block;
      width: 100%;
      height: 3px;
      background-color: #000;
      border-radius: 3px;
      transition: transform .6s ease-in-out, top .6s ease-in-out;
    }
    ::before {
      top: -12px;
      transform: ${({open}) => open ? "rotate(45deg) translate3d(9px, 8px, 0)" : "rotate(0) translate3d(0, 0, 0)"};
    }
    ::after {
      top: 12px;
      transform: ${({open}) => open ? "rotate(-45deg) translate3d(8px, -10px, 0)" : "rotate(0) translate3d(0, 0, 0)"};
    }
  }
}
.link-close {
  position: absolute;
  top: 18px;
  right: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 34px;
  height: 34px;
  padding: 0;
  border: 0;
  background-color: transparent;
  z-index: 1;
  span {
    position: relative;
    display: block;
    width: 100%;
    height: 3px;
    background-color: transparent;
    border-radius: 3px;
    transition: background-color .6s;
    ::before, ::after {
      content: '';
      position: absolute;
      left: 0;
      display: block;
      width: 100%;
      height: 3px;
      background-color: #000;
      border-radius: 3px;
      transition: transform .6s ease-in-out, top .6s ease-in-out;
    }
    ::before {
      top: -12px;
      transform: rotate(45deg) translate3d(8px, 8px, 0);
    }
    ::after {
      top: 12px;
      transform: rotate(-45deg) translate3d(8px, -10px, 0);
    }
  }
}
  @media (min-width: 1024px) {
    display: flex;
    align-items: center;
    padding: 20px 30px;
    .burger-button {
      display: flex;
      right: 30px;
      top: 29px;
      :hover {
        cursor: pointer;
      }
    }
    .link-close {
      top: 85px;
      right: 34px;
      background-color: #fff;
      span {
        width: 73%;
      }
    }
  }
`;

const LinkLogo = styled.a`
position: relative;
display: inline-block;
width: 116px;
height: 34px;
z-index: 2;
svg {
  width: 100%;
  height: 100%;
}
  @media (min-width: 1024px) {
    width: 171px;
    height: 45px;
    :hover {
      cursor: pointer;
    }
  }
`;