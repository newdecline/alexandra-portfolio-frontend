import React, { useState, useRef, forwardRef } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import ArrowIcon from '../svg/icon-arrow.svg';
import {filterPoints} from '../fakeData/filterPoints';
import {Collapsing} from "./Collapsing";
import {setAllProjects} from "../redux/actions/actionCreators";

export const ProjectFilter = forwardRef((props, ref) => {
    const dropDownListRef = useRef(null);

    const dispatch = useDispatch();

    const {selectedPoint, setSelectedPoint} = props;

    const [isOpenFilter, setOpenFilter] = useState(false);

    const handleClickFilterButton = () => {
        setOpenFilter(!isOpenFilter);
    };

    const handleClickDropDownListItem = (id) => {
        setOpenFilter(false);
        setSelectedPoint(filterPoints[id].label);
        dispatch(setAllProjects(false));
    };

    return (
        <>
            <ProjectFilterButton ref={ref} onClick={handleClickFilterButton} open={isOpenFilter}>
                {!selectedPoint ? 'Категории' : selectedPoint}
                <ArrowIconButton open={isOpenFilter}>
                    <ArrowIcon/>
                </ArrowIconButton>
            </ProjectFilterButton>

            <Collapsing isCollapsed={isOpenFilter} classes={['drop-down-list']}>
                <DropDownList ref={dropDownListRef}>
                    {
                        filterPoints.map(item => {
                            const {id, label} = item;

                            return (
                                <DropDownListItem
                                    key={id}
                                    onClick={() => handleClickDropDownListItem(id)}
                                    active={selectedPoint === label}>
                                    {label}
                                </DropDownListItem>
                            )
                        })
                    }
                </DropDownList>
            </Collapsing>
        </>
    )
});

const ProjectFilterButton = styled.button`
  @media (min-width: 320px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: calc(100% - 32px);
    margin: ${({open}) => open ? "0 auto" : "0 auto 16px auto"};
    padding: 11px 12px 11px 22px;
    font-size: 20px;
    line-height: 29px;
    color: #000000;
    box-sizing: border-box;
    background-color: #EDEDED;
    border: none;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    border-bottom-left-radius: ${({open}) => open ? "0" : "5px"};
    border-bottom-right-radius: ${({open}) => open ? "0" : "5px"};
    transition: margin-bottom .6s ease-in-out, border .6s ease-in-out;
  }
  @media (min-width: 1024px) {
    display: none;
  }
`;

const ArrowIconButton = styled.span`
  @media (min-width: 320px) {
    display: flex;
    width: 17px;
    height: 9px;
    transform: ${({open}) => open ? "scale(1, -1)" : "scale(1, 1)"};
    transition: transform .6s;
    svg {
      width: 100%;
      height: auto;
    }
  }
`;

const DropDownList = styled.ul`
  @media (min-width: 320px) {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    width: calc(100% - 32px);
    margin: 0 auto 17px auto;
    padding: 11px 12px 11px 22px;
    font-size: 20px;
    line-height: 29px;
    color: #000;
    box-sizing: border-box;
    background-color: #EDEDED;
    border: none;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
  @media (min-width: 1024px) {
    flex-direction: row;
    justify-content: space-between;
    width: calc(100% - 160px);
    background-color: #fff;
  }
  @media (min-width: 1380px) {
    width: 63%;
  }
`;

const DropDownListItem = styled.li`
  @media (min-width: 320px) {
    padding: 6px 0;
  }
  @media (min-width: 1024px) {
    color: ${({active}) => active ? '#1ABC9C' : '#000'};
    font-weight: ${({active}) => active ? '600' : '400'};
    transition: color .6s;
    :hover {
      cursor: pointer;
      color: #1ABC9C;
    }
  }
`;