import React, {useState, useEffect, useRef} from 'react';
import {useSelector} from 'react-redux';
import styled, {keyframes} from 'styled-components';
import Link from 'next/link';
import {projectsListFakeData} from '../fakeData/projectList';
import uuid from 'uuid/v4';
import Img from 'react-image';
import {AllProjectsButton} from "./AllProjectsButton";
import SvgLoader from '../svg/svg-loaders.svg';

export const Projects = () => {
    let projectsToShow = 3;

    const projectListRef = useRef(null);

    const isShowAllProjects = useSelector(({projects}) => projects.isShowAllProjects);

    const [projectsList, setProjectsList] = useState(projectsListFakeData.slice(0, projectsToShow));

    if (process.browser && window.matchMedia("(min-width: 1024px)").matches) {
        projectsToShow = 4;
    }

    useEffect(() => {
        projectListRef.current && projectListRef.current.classList.add('initialized');
    }, []);

    useEffect(() => {
        isShowAllProjects ? setProjectsList(projectsListFakeData) : setProjectsList((projectsListFakeData.slice(0, projectsToShow)));
    }, [isShowAllProjects]);

    return (
        <Section data-id-section="projects">
            <ul ref={projectListRef} className="projects-list">
                {
                    projectsList.map(project => {
                        const {slug, alt, preview, titleInHtml, categories} = project;

                        return (
                            <li key={uuid()} className="list-item">
                                <Link
                                    href={{
                                        pathname: '/project',
                                        query: {slug}
                                    }}
                                    as={`/proekt/${slug}`}>
                                    <LinkItem>
                                        <Img
                                            src={preview}
                                            loader={<div className="svg-wrap"><SvgLoader/></div>}
                                            alt={alt}/>
                                    </LinkItem>
                                </Link>
                                <div className="content-hover">
                                    <h2 className="content-hover__title">{titleInHtml}</h2>
                                    <p className="content-hover__categories">{categories.join(', ')}</p>
                                </div>
                            </li>
                        )
                    })
                }
            </ul>

            <AllProjectsButton
                disabled={projectsListFakeData.length <= projectsToShow}
                projectListRef={projectListRef}/>
        </Section>
    )
};


const fadeIn = keyframes`
  0% { 
    opacity: 0;
    transform: scale(.9, .9);
  }
  100% { 
    opacity: 1;
    transform: scale(1, 1);
  }
`;

const preloaderRotation = keyframes`
  0% { 
    transform: rotate(0);
  }
  100% { 
    transform: rotate(358deg);
  }
`;

const Section = styled.section`
  padding-top: 64px;
  .projects-list {
    display: flex;
    flex-direction: column;
    .list-item {
      position: relative;
      animation: ${fadeIn} .6s ease-in-out;
      display: none;
      :nth-child(1),
      :nth-child(2),
      :nth-child(3) {
        display: block;
      }
    }
    &.initialized {
      .list-item {
        display: block;
        :hover {
          cursor: pointer;
          .content-hover {
            opacity: 1;
          }
        }
      }
    }
  }
  .content-hover {
      display: none;
      position: absolute;
      top: 0;
      left: 0;
      padding: 0 55px;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, .8);
      color: #fff;
      text-align: center;
      box-sizing: border-box;
      opacity: 0;
      transition: opacity .6s;
      pointer-events: none;
      &__title {
        margin-bottom: 30px;
        font-size: 22px;
        line-height: 35px;
        white-space: pre-wrap;
      }
      &__categories {
        font-size: 18px;
        line-height: 29px;
        text-transform: lowercase;
        :first-letter {
          text-transform: uppercase;
        }
      }
    }
  @media (min-width: 1024px) {
    padding-top: 85px;
    .projects-list {
      flex-direction: row;
      flex-wrap: wrap;
    }
    .content-hover {
      display: flex;
    }
    .list-item {
      width: 50%;
      :hover {
        .content-hover {
          opacity: 1;
        }
      }
    }
  }
  @media (min-width: 1380px) {
    max-width: 1366px;
    margin: 0 auto;
  }
`;

const LinkItem = styled.a`
  position: relative;
  top: 0;
  left: 0;
  padding-top: 48%;
  display: block;
  width: 100%;
  .svg-wrap {
    position: absolute;
    top: 50%;
    left: 50%;
    object-fit: cover;
    display: block;
    width: 38px;
    height: 38px;
    transform: translate(-18px, -18px);
  }
  img {
    position: absolute;
    top: 0;
    left: 0;
    object-fit: cover;
    display: block;
    width: 100%;
    height: 100%;
  }
  svg {
    animation: ${preloaderRotation} .6s ease-in-out infinite;
  }
`;