import React from 'react';
import styled from 'styled-components';

export const SectionHeader = props => {
    const {title} = props;

    return (
        <Header>{title}</Header>
    )
};

const Header = styled.h2`
  @media (min-width: 320px) {
    margin: 0;
    padding: 30px 0;
    font-size: 20px;
    line-height: 29px;
    text-transform: uppercase;
    text-align: center;
    font-family: 'Circe-Bold';
  }
  @media (min-width: 1024px) {
    margin: 0;
    padding: 45px 0;
    font-size: 28px;
    line-height: 41px;
  }
  @media (min-width: 1366px) {
    margin: 0;
    padding: 35px 0;
    font-size: 28px;
    line-height: 41px;
  }
`;