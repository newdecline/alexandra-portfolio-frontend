import React from 'react';
import styled from 'styled-components';
import InstagramIcon from '../svg/icon-instagram.svg';
import VkIcon from '../svg/icon-vk.svg';
import BeehanceIcon from '../svg/icon-beehance.svg';

export const SocialLinkList = props => {
    const {isOpenMenu} = props;

    return (
        <StyledSocialLinkList isOpen={isOpenMenu}>
            {
                socialList.map((link, index) => {
                    const {icon, href} = link;

                    return (
                        <li className="list-item" key={index}>
                            <a className="list-link" href={href} target="_blank">{icon}</a>
                        </li>
                    )
                })
            }
        </StyledSocialLinkList>
    )
};

const StyledSocialLinkList = styled.ul`
  display: flex;
  margin-top: 107px;
  .list-item {
    display: block;
    margin-right: 29px;
    :last-child {
      margin-right: 0;
    }
  }
  .list-link {
    display: flex;
    width: 40px;
    height: 40px;
    svg {
      width: 100%;
      height: 100%;
    }
  }
  @media (min-width: 1024px) {
    position: fixed;
    bottom: 0;
    right: ${({isOpen}) => isOpen ? '0' : '-100%'};
    height: calc((var(--vh, 1vh) * 100) - 85px);
    margin-top: 0;
    padding: 0 30px;
    flex-direction: column;
    background-color: #fff;
    transition: right .6s ease-in-out;
    z-index: 2;
    .list-item {
      margin-bottom: 19px;
      margin-right: 0;
    }
  }
`;

const socialList = [
    {
        icon: <InstagramIcon/>,
        href: 'http://instagram.com/strakhovaaleksandra'
    },
    {
        icon: <VkIcon/>,
        href: 'https://vk.com/id153726265'
    },
    {
        icon: <BeehanceIcon/>,
        href: 'https://www.behance.net/austrahovad9ef'
    }
];
