import React from 'react';
import TextareaAutosize from "react-textarea-autosize";

export const CustomTextarea = ({ field: {name, value, onChange}, form, ...props }) => {
    if (process.browser && window.matchMedia( "(min-width: 1024px)" ).matches) {
        return (
            <textarea
                onChange={onChange}
                cols="30"
                rows={3}
                className="form__textarea"
                name={name}
                placeholder="О проекте"
                value={value}></textarea>
        )
    }

    return (
        <TextareaAutosize
            onChange={onChange}
            cols="30"
            rows={3}
            minRows={3}
            className="form__textarea"
            name={name}
            placeholder="О проекте"
            value={value}/>
    )
};