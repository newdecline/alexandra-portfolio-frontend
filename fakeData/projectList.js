export const projectsListFakeData = [
    {
        id: 0,
        slug: 'veshevorot',
        preview: '/images/projects/preview/001.jpg',
        fullImg: '/images/projects/full/001.jpg',
        headerBg: '/images/projects/header-bg/001.jpg',
        alt: 'Вещеворот',
        title: 'Редизайн логотипа и фирменного стиля социального проекта "Вещеворот"',
        titleInHtml: `Редизайн логотипа и фирменного стиля
социального проекта "Вещеворот"`,
        categories: ['Логотип', 'Брендинг'],
        textColor: '#fff'
    },
    {
        id: 1,
        slug: 'korobochka',
        preview: '/images/projects/preview/002.jpg',
        fullImg: '/images/projects/full/002.jpg',
        headerBg: '/images/projects/header-bg/002.jpg',
        alt: 'Коробочка',
        title: 'Разработка логотипа магазина подарков "Бери Дари"',
        titleInHtml: `Разработка логотипа
магазина подарков "Бери Дари"`,
        categories: ['Логотип'],
        textColor: '#fff'
    },
    {
        id: 2,
        slug: 'oblaka',
        preview: '/images/projects/preview/003.jpg',
        fullImg: '/images/projects/full/003.jpg',
        headerBg: '/images/projects/header-bg/003.jpg',
        alt: 'Облака',
        title: 'Разработка логотипа для детского центра "Облака"',
        titleInHtml: `Разработка логотипа
для детского центра "Облака"`,
        categories: ['Логотип'],
        textColor: '#fff'
    },
    {
        id: 11,
        slug: 'etiketki-upakovki-batonchikov',
        preview: '/images/projects/preview/011.jpg',
        fullImg: '/images/projects/full/011.jpg',
        headerBg: '/images/projects/header-bg/011.jpg',
        alt: 'Этикетки для батончиков и гранолы',
        title: 'Разработка упаковки для злаковых батончиков и гранолы',
        titleInHtml: `Разработка упаковки
для злаковых батончиков и гранолы`,
        categories: ['Упаковка'],
        textColor: '#fff'
    },
    {
        id: 14,
        slug: 'ziloy-komllex',
        preview: '/images/projects/preview/014.jpg',
        fullImg: '/images/projects/full/014.jpg',
        headerBg: '/images/projects/header-bg/014.jpg',
        alt: 'Разработка концепции инстаграмма для жилого комплекса',
        title: ' Разработка концепции инстаграмма для жилого комплекса',
        titleInHtml: `Разработка концепции инстаграмма для жилого комплекса`,
        categories: ['Дизайн социальных сетей'],
        textColor: '#000'
    },
    {
        id: 16,
        slug: 'logotip-poligraphia-mastera-manikura',
        preview: '/images/projects/preview/016.jpg',
        fullImg: '/images/projects/full/016.jpg',
        headerBg: '/images/projects/header-bg/016.jpg',
        alt: 'Логотип и полиграфия для масетра маникюра',
        title: ' Разработка логотипа и полиграфии для мастера маникюра',
        titleInHtml: `Разработка логотипа и полиграфии для мастера маникюра`,
        categories: ['Логотип и полиграфия'],
        textColor: '#fff'
    },
    {
        id: 12,
        slug: 'welness',
        preview: '/images/projects/preview/012.jpg',
        fullImg: '/images/projects/full/012.jpg',
        headerBg: '/images/projects/header-bg/012.jpg',
        alt: ' Логотип и фирменный стиль стоматологии wellness',
        title: ' Разработка логотипа и фирменного стиля стоматологии Wellness',
        titleInHtml: `Разработка логотипа 
и фирменного стиля стоматологии Wellness`,
        categories: ['Логотип и фирменный стиль'],
        textColor: '#fff'
    },
    {
        id: 13,
        slug: 'meat-feast',
        preview: '/images/projects/preview/013.jpg',
        fullImg: '/images/projects/full/013.jpg',
        headerBg: '/images/projects/header-bg/013.jpg',
        alt: ' Логотип и упаковка для мяса “Meat feast”',
        title: '  Разработка логотипа и упаковки для мяса “Meat feast”',
        titleInHtml: `Разработка логотипа и упаковки для мяса “Meat feast”`,
        categories: ['Логотип и фирменный стиль'],
        textColor: '#fff'
    },
    {
        id: 15,
        slug: 'logofolio-2018/19',
        preview: '/images/projects/preview/015.jpg',
        fullImg: '/images/projects/full/015.jpg',
        headerBg: '/images/projects/header-bg/006.jpg',
        alt: 'Logofolio 2018/ 19',
        title: 'Logofolio 2018/ 19',
        titleInHtml: 'Logofolio 2018/ 19',
        categories: ['Логотип'],
        textColor: '#fff'
    },
    {
        id: 3,
        slug: 'belim-belo',
        preview: '/images/projects/preview/004.jpg',
        fullImg: '/images/projects/full/004.jpg',
        headerBg: '/images/projects/header-bg/004.jpg',
        alt: 'Белым бело',
        title: ' Разработка торговой марки муки "Белым бело"',
        titleInHtml: `Разработка торговой марки 
муки "Белым бело"`,
        categories: ['Логотип', 'Брендинг'],
        textColor: '#fff'
    },
    {
        id: 4,
        slug: 'sol-bereg',
        preview: '/images/projects/preview/005.jpg',
        fullImg: '/images/projects/full/005.jpg',
        headerBg: '/images/projects/header-bg/005.jpg',
        alt: 'Sol bereg',
        title: 'Разработка логотипа для туристической компании "Солнечный берег"',
        titleInHtml: `Разработка логотипа для туристической компании 
"Солнечный берег"`,
        categories: ['Логотип', 'Полиграфия'],
        textColor: '#fff'
    },
    {
        id: 5,
        slug: 'logofolio-2016/17',
        preview: '/images/projects/preview/006.jpg',
        fullImg: '/images/projects/full/006.jpg',
        headerBg: '/images/projects/header-bg/006.jpg',
        alt: 'Logofolio 2016/ 17',
        title: 'Logofolio 2016/ 17',
        titleInHtml: 'Logofolio 2016/ 17',
        categories: ['Логотип'],
        textColor: '#fff'
    },
    {
        id: 6,
        slug: 'magazin-odezdy',
        preview: '/images/projects/preview/007.jpg',
        fullImg: '/images/projects/full/007.jpg',
        headerBg: '/images/projects/header-bg/007.jpg',
        alt: 'Магазин одежды',
        title: 'Разработка сайта интернет магазина одежды',
        titleInHtml: `Разработка сайта 
интернет магазина одежды`,
        categories: ['Веб дизайн'],
        textColor: '#fff'
    },
    {
        id: 7,
        slug: 'persona',
        preview: '/images/projects/preview/008.jpg',
        fullImg: '/images/projects/full/008.jpg',
        headerBg: '/images/projects/header-bg/008.jpg',
        alt: 'Наружная реклама',
        title: 'Наружная реклама (биллборды, вывески)',
        titleInHtml: `Наружная реклама 
(биллборды, вывески)`,
        categories: ['Наружная реклама'],
        textColor: '#fff'
    },
    {
        id: 8,
        slug: 'more-produkty',
        preview: '/images/projects/preview/009.jpg',
        fullImg: '/images/projects/full/009.jpg',
        headerBg: '/images/projects/header-bg/009.jpg',
        alt: 'Море продукты',
        title: 'Разработка сайта интернет магазина морепродуктов',
        titleInHtml: `Разработка сайта 
интернет магазина морепродуктов`,
        categories: ['Веб дизайн'],
        textColor: '#fff'
    },
    {
        id: 11,
        slug: 'posadochnie-stranicy-saitov',
        preview: '/images/projects/preview/010.jpg',
        fullImg: '/images/projects/full/010.jpg',
        headerBg: '/images/projects/header-bg/010.jpg',
        alt: 'Главные экраны сайтов',
        title: 'Разработка главных экранов для сайтов',
        titleInHtml: `Разработка главных экранов 
для сайтов`,
        categories: ['Веб дизайн'],
        textColor: '#fff'
    }
];