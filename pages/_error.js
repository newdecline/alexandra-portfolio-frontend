import React from 'react'

function Error({ statusCode }) {
    return (
        <div className="error-page">
            {statusCode}
        </div>
    )
}

Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
    return { statusCode }
};

export default Error