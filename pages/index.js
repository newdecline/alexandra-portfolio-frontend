import React, { useEffect } from 'react';
import scrollIntoView from 'scroll-into-view';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import {withRedux} from "../redux/withRedux";
import {Projects} from '../components/Projects';
import {Feedback} from "../components/Feedback";
import {Contacts} from "../components/Contact";
import {setPageIndexOffsetBeforeChange, setScrollTo} from "../redux/actions/actionCreators";

const Home = () => {
    const {pageIndexOffsetBeforeChange, scrollTo} = useSelector(({pageIndex}) => pageIndex);
    const dispatch = useDispatch();

    const router = useRouter();

    const onBeforeChangeRote = () => {
        dispatch(setPageIndexOffsetBeforeChange(window.pageYOffset));
    };

    useEffect(() => {
        router.events.on('beforeHistoryChange', onBeforeChangeRote);

        const target = document.querySelector(`[data-id-section=${scrollTo}]`);

        if (scrollTo) {
            scrollIntoView(target, {
                time: 100,
                align: {
                    top: 0,
                    left: 0,
                    topOffset: 70,
                    leftOffset: 0
                }
            });
            dispatch(setScrollTo(null));
        } else {
            setTimeout(() => {window.scrollTo(0, pageIndexOffsetBeforeChange);}, 100);
        }

        return () => {
            router.events.off('beforeHistoryChange', onBeforeChangeRote);
        }
    }, []);

    return (
        <>
            <Projects/>
            <Feedback/>
            <Contacts/>
        </>
    )
};



export default withRedux(Home, {classes: ['page-index']});
