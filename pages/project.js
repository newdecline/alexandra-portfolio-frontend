import React, {useState} from 'react';
import Head from "next/dist/next-server/lib/head";
import {withRedux} from "../redux/withRedux";
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import {PageProjectLayout} from "../components/Layouts/PageProjectLayout";
import Link from 'next/link';
import styled, {keyframes} from 'styled-components';
import Img from 'react-image';
import VisibilitySensor from 'react-visibility-sensor';
import SvgLoader from '../svg/svg-loaders.svg';
import {projectsListFakeData} from '../fakeData/projectList';
import IconArrow from '../svg/icon-arrow.svg';
import {setPageIndexOffsetBeforeChange, setScrollTo} from "../redux/actions/actionCreators";

const Project = props => {
    const dispatch = useDispatch();
    const router = useRouter();

    const {projectIndex} = props;

    const [isVisibleFooter, setVisibleFooter] = useState(false);
    const [currentProjectIndex, setCurrentProjectIndex] = useState(projectIndex);

    const {alt, headerBg, categories, fullImg, textColor, titleInHtml} = projectsListFakeData[currentProjectIndex];

    const handleClickMenuItem = (e) => {
        e.preventDefault();

        dispatch(setPageIndexOffsetBeforeChange(0));
        dispatch(setScrollTo('contacts'));
        router.push('/');
    };

    const onChange = isVisible => {
        setVisibleFooter(isVisible);
    };

    const prevProject = () => {
        currentProjectIndex !== 0 && setCurrentProjectIndex(currentProjectIndex - 1);
    };

    const nextProject = () => {
        projectsListFakeData.length -1 !== currentProjectIndex && setCurrentProjectIndex(currentProjectIndex + 1);
    };

    return (
        <>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <link rel="icon" type="image/x-icon" href="/favicon.png" />
                <title>Портфолио. Проект {projectsListFakeData[currentProjectIndex].title}</title>
                <meta
                    name="description"
                    content="Создам дизайн любого уровня"/>
                <meta
                    name="keywords"
                    content="Дизайн, графический дизайнер, дизайнер, дизайнер в Челябинске, заказать логотип, заказать сайт, заказать визитку, закать листовку, заказать баннер, наружняя реклама"/>
            </Head>

            <StyledProjectPage bg={headerBg} textColor={textColor} isVisibleFooter={isVisibleFooter}>
                <div className="header">
                    <h3 className="header__title">{titleInHtml}</h3>
                    <h6 className="header__subtitle">— <span>{categories.join(', ')}</span> —</h6>
                </div>
                <div className="wrapper-img">
                    <Img
                        src={fullImg}
                        loader={<div className="svg-wrap"><SvgLoader/></div>}
                        className="img"
                        alt={alt}/>
                </div>
                <VisibilitySensor onChange={onChange}>
                    <div className="footer">
                        <h6 className="footer__title">Оставьте заявку на разработку дизайна</h6>
                        <a
                            onClick={(e) => handleClickMenuItem(e)}
                            className="link">Хочу дизайн</a>
                    </div>
                </VisibilitySensor>
                <div className="links-nav">
                    {currentProjectIndex !== 0 &&
                    <Link
                        href={{
                            pathname: '/project',
                            query: {slug: projectsListFakeData[currentProjectIndex - 1].slug}
                        }}
                        as={`/proekt/${projectsListFakeData[currentProjectIndex - 1].slug}`}>
                        <a onClick={prevProject} className="links-nav__item prev"><IconArrow/></a>
                    </Link>}
                    {projectsListFakeData.length - 1 !== currentProjectIndex &&
                    <Link
                        href={{
                            pathname: '/project',
                            query: {slug: projectsListFakeData[currentProjectIndex + 1].slug}
                        }}
                        as={`/proekt/${projectsListFakeData[currentProjectIndex + 1].slug}`}>
                        <a onClick={nextProject} className="links-nav__item next"><IconArrow/></a>
                    </Link>}
                </div>
            </StyledProjectPage>
        </>
    )
};

Project.getInitialProps = async ({query}) => {
    const projectIndex = projectsListFakeData.findIndex(project => project.slug === query.slug);

    return {projectIndex}
};

export default withRedux(Project, {Layout: PageProjectLayout, classes: ['page-project']});

const preloaderRotation = keyframes`
  0% { 
    transform: rotate(0);
  }
  100% { 
    transform: rotate(358deg);
  }
`;

const StyledProjectPage = styled.div`
padding-top: 64px;
position: relative;
.header {
  padding: 22px 26px;
  background: ${({bg}) => `url(${bg})`};
  color: ${({textColor}) => textColor};
  text-align: center;
  &__title {
    margin-bottom: 17px;
    font-size: 16px;
    line-height: 24px;
    text-transform: uppercase;
    font-weight: 700;
  }
  &__subtitle {
    font-size: 16px;  
    line-height: 24px;
    span {
      display: inline-block;
      text-transform: lowercase;
      ::first-letter {
        text-transform: uppercase;
      }
    }
  }
}
.wrapper-img {
  position: relative;
  min-height: 100vh;
}
.img {
  max-width: 100%;
  display: block;
}
.svg-wrap {
  position: absolute;
  top: 10%;
  left: 50%;
  object-fit: cover;
  display: block;
  width: 38px;
  height: 38px;
  transform: translate(-18px, -18px);
  svg {
    animation: ${preloaderRotation} .6s ease-in-out infinite;
  }
}
.footer {
  padding: 25px 20px;
  text-align: center;
  &__title {
    margin-bottom: 10px;
    font-size: 16px;
    line-height: 24px;
    font-weight: 700;
  }
  .link {
    display: inline-block;
    padding: 17px 85px;
    background: #1ABC9C;
    text-decoration: none;
    border-radius: 5px;
    color: #fff;
  }
}
.links-nav {
  display: none;
}
@media (min-width: 1024px) {
  padding-top: 76px;
  .header {
    padding: 42px 26px;
    &__title {
    font-size: 24px;
    line-height: 35px;
    }
    &__subtitle {
      font-size: 22px;
      line-height: 32px;
    }
  }
  .footer {
  padding: 45px 20px;
    &__title {
      font-size: 20px;
      line-height: 29px
    }
    .link {
      font-size: 20px;
      line-height: 29px;
      &:hover {
        cursor: pointer;
      }
    }
  }
  .links-nav {
    display: ${({isVisibleFooter}) => isVisibleFooter ? 'none' : 'flex'};
    width: 100%;
    justify-content: space-between;
    position: fixed;
    top: 50vh;
    left: 0;
    &__item {
      display: flex;
      width: 60px;
      height: 60px;
      align-items: center;
      justify-content: center;
      background-color: #fff;
      box-sizing: border-box;
      svg {
        width: 65%;
        height: 65%;
      }
      &.prev {
        transform: rotate(90deg);
      }
      &.next {
        transform: rotate(-90deg);
        margin-left: auto;
      }
    }
  }
}
`;