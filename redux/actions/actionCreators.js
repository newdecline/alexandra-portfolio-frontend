import {actionTypes} from "../actions/actionTypes";

export const setAllProjects = payload => {
    return { type: actionTypes.SET_SHOW_ALL_PROJECTS, payload }
};

export const setPageIndexOffsetBeforeChange = payload => {
    return { type: actionTypes.SET_PAGE_INDEX_OFFSET_BEFORE_CHANGE, payload }
};

export const setScrollTo = payload => {
    return { type: actionTypes.SET_SCROLL_TO, payload }
};