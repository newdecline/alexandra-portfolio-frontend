import { combineReducers } from "redux";
import { projects } from './projects';
import { pageIndex } from './pageIndex';

export default combineReducers({
    projects,
    pageIndex
});