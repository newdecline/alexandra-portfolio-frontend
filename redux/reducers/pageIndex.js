import {actionTypes} from "../actions/actionTypes";

const initialState = {
    pageIndexOffsetBeforeChange: 0,
    scrollTo: null

};

export const pageIndex = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_PAGE_INDEX_OFFSET_BEFORE_CHANGE:
            return {...state, pageIndexOffsetBeforeChange: action.payload};
        case actionTypes.SET_SCROLL_TO:
            return {...state, scrollTo: action.payload};
        default:
            return state
    }
};