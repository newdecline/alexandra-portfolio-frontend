import {actionTypes} from "../actions/actionTypes";

const initialState = {
    isShowAllProjects: false
};

export const projects = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.SET_SHOW_ALL_PROJECTS:
            return {...state, isShowAllProjects: action.payload};
        default:
            return state
    }
};