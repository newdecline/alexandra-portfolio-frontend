import React, {useEffect} from 'react';
import App from 'next/app';
import parse from 'html-react-parser';
import {useRouter} from "next/router";
import {Provider} from 'react-redux';
import {initializeStore} from './store'
import {MainLayout} from "../components/Layouts/MainLayout";
import {Div100Vh} from "../components/Div100Vh";
import {omit, pick} from 'lodash';
import reset from 'styled-reset';
import {createGlobalStyle} from 'styled-components';
import '../scss/index.scss';
import Head from "next/head";

export const withRedux = (
    PageComponent, {ssr = true, Layout = MainLayout, fetchURLLayout = '/layout', classes = []} = {}) => {

    const WithRedux = ({initialReduxState, ...props}) => {
        if (props.error && props.error.statusCode === 404 || props.statusCode === 404) {
            const err = new Error();
            err.statusCode = 404;
            err.code = 'ENOENT';
            throw err;
        }

        const store = getOrInitializeStore(initialReduxState);
        const router = useRouter();

        const handleRouteChange = url => {
            if (history.state.as !== url) {
                typeof Ya === 'function' && Ya.Metrika2.counters().forEach(({id}) => {
                    ym(id, 'hit', url);
                });
            }
        };

        useEffect(() => {
            router.events.on('beforeHistoryChange', handleRouteChange);

            return () => {
                router.events.off('beforeHistoryChange', handleRouteChange);
            }
        }, []);

        return (
            <>
                <Head>
                    {parse(`
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(56612137, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/56612137" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
`)}
                </Head>

                <Provider store={store}>
                    <GlobalStyle/>
                    <Div100Vh>
                        <div className={classes.join(' ')}>
                            <Layout {...pick(props, 'layoutData')}>
                                <PageComponent {...props} />
                            </Layout>
                        </div>
                    </Div100Vh>
                </Provider>
            </>
        )
    };

    if (process.env.NODE_ENV !== 'production') {
        const isAppHoc =
            PageComponent === App || PageComponent.prototype instanceof App;
        if (isAppHoc) {
            throw new Error('The withRedux HOC only works with PageComponents')
        }
    }

    if (process.env.NODE_ENV !== 'production') {
        const displayName =
            PageComponent.displayName || PageComponent.name || 'Component';

        WithRedux.displayName = `withRedux(${displayName})`
    }

    if (ssr || PageComponent.getInitialProps) {
        WithRedux.getInitialProps = async context => {
            const reduxStore = getOrInitializeStore();

            context.reduxStore = reduxStore;

            let pageProps =
                typeof PageComponent.getInitialProps === 'function'
                    ? await PageComponent.getInitialProps(context)
                    : {};

            return {
                ...pageProps,
                initialReduxState: reduxStore.getState()
            }
        }
    }

    return WithRedux
};

let reduxStore;

const getOrInitializeStore = initialState => {
    if (typeof window === 'undefined') {
        return initializeStore(initialState)
    }

    if (!reduxStore) {
        reduxStore = initializeStore(initialState)
    }

    return reduxStore
};

const GlobalStyle = createGlobalStyle`
  ${reset}
  
  body {
        font-family: 'Circe-Regular';
        overflow-x: hidden;
    }

    @font-face {
        font-family: 'Circe-Light';
        src: url('/fonts/Circe-Light.woff');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: 'Circe-Regular';
        src: url('/fonts/Circe-Regular.woff');
        font-weight: 500;
        font-style: normal;
    }

    @font-face {
        font-family: 'Circe-Bold';
        src: url('/fonts/Circe-Bold.woff');
        font-weight: 700;
        font-style: normal;
    }
`;