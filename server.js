const express = require('express');
const next = require('next');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 3000;
// const proxy = require('http-proxy-middleware');
//
// const optionsProxy = proxy({
// 	target: process.env.BASE_URL,
// 	changeOrigin: true
// });

app.prepare().then(() => {
    const server = express();

    // server.use(['/api', '/admin'], optionsProxy);
    dev && server.use(express.static('public'));

    server.get('/', (req, res) => {
        const actualPage = '/index';
        app.render(req, res, actualPage);
    });

    server.get('/proekt/:slug', (req, res) => {
        const actualPage = '/project';
        const queryParams = { slug: req.params.slug };
        app.render(req, res, actualPage, queryParams);
    });

    server.get('*', (req, res) => handle(req, res));

    const listenCallback = err => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`)
    };

    dev ? server.listen(port, listenCallback) : server.listen(port, 'localhost', listenCallback);
})
.catch(ex => {
    console.error(ex.stack);
    process.exit(1);
});